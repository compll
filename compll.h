/* compll - A compressed memory allocator

  Copyright (c) 2010 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#ifndef _compll_h_
#define _compll_h_


/* these types provide pointers to the (de)compression functions */
typedef unsigned int (*compll_compress_callback)(
   const unsigned char *, unsigned int,
         unsigned char *, unsigned int);
typedef void (*compll_decompress_callback)(
   const unsigned char *, unsigned int,
         unsigned char *, unsigned int);
 

#ifndef COMPLL_NOLIB

/* the initialization function */
int compll_init(unsigned int   block_size,
                unsigned short alignment,
                unsigned short uncomp_count,
                compll_compress_callback   compress_cb,
                compll_decompress_callback decompress_cb);


/* the data type to identify a node, a 64 bits integer */
typedef unsigned long long compll_t;

/* create a node of a specified size (in bytes) */
compll_t compll_alloc(unsigned int size);

/* remove a node */
void compll_free(compll_t node);


/* returns a pointer to the node data for reading */
const void *compll_read(compll_t node);

/* the same, for writing */
void *compll_write(compll_t node);



#else 

/* we can use regular pointers to identify nodes */
typedef void * compll_t;

/* nothing has to be initialized,
   so the init function is replaced with a no-op */
#define compll_init(bs, al, cnt, comp, uncomp) 0

/* allocating a new node is equivalent to calloc() */
#define compll_alloc(size) calloc(size, 1)
/* ...and freeing a node is quite simple */
#define compll_free(p) free(p)

/* nothing has to be done when accessing nodes,
   as we already work with regular memory pointers */
#define compll_read(p)  (p)
#define compll_write(p) (p)

#endif


#endif
