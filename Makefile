
.PHONY: test clean

compll.o: compll.c compll.h
	gcc -Wall -Wextra -g -c compll.c

compll: compll.c compll.h
	gcc -Wall -Wextra -g -DDEBUG compll.c -o compll
	./compll

clean:
	rm -f compll.o compll
	${MAKE} -C test clean

test:
	${MAKE} -C test test

